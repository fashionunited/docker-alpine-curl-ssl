# docker alpine curl ssl

Alpine image with curl + ca-certificates, useful for API requests

## Usage

docker run fuww/alpine-curl-ssl curl https://api.exaple.com/

## Usage with jq

docker run fuww/alpine-curl-ssl-jq sh -c "curl https://api.exaple.com/ | jq .something"
